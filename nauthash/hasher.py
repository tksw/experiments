import click
import hashlib
import os

ALGO_LIST = {
    "md5": hashlib.md5,
    "sha1": hashlib.sha1,
    "sha256": hashlib.sha256,
}


@click.command()
@click.option(
    "--algo",
    default="md5",
    help="Which hashing algorithm? (MD5, SHA1, SHA256)")
@click.option(
    "--output", default=None, help="Where you want to output checksum file")
@click.argument("files", nargs=-1, type=click.Path(exists=True))
def hashit(algo, output, files):
    clean_algo = algo.lower().strip() 
    func = ALGO_LIST.get(clean_algo, False)
    if not func:
        raise click.ClickException.show(hashit)

    for path in files:
        if(os.path.isdir(path)):
            func = dir_wrapper(func)
            hash_list = func(path)
            print("\n".join(hash_list))
        else:
            func = file_wrapper(func)
            hash_list = func(path)
            print(hash_list)

def dir_wrapper(given_func):
    def dirwrap(path):
        sums = []
        items = os.scandir(path)
        sub_files = [entry for entry in items if entry.is_file()]
        for a_file in sub_files:
            given_file = open(a_file.path, "rb").read()
            sums.append(given_func(given_file).hexdigest() + " " + a_file.name)
        return sums
    return dirwrap

def file_wrapper(given_func):
    def filewrap(path):
        given_file = open(path, "rb").read()
        return given_func(given_file).hexdigest() + " " + path + "\n"
    return filewrap


if __name__ == "__main__":
    hashit()
