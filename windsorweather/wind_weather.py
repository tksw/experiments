import matplotlib.dates
import numpy as np
import pandas as pd
# import scipy
# import scipy.stats
from matplotlib import pyplot as plt
import datetime

data_frame = pd.read_csv("./wdata.csv", header=16)

x_data = []
y_data = []
average = {}


def tempchange(arr):
    temp = []
    for i in arr:
        temp.append(datetime.date(i, 6, 6))
    return temp


for index, value in enumerate(data_frame["Date/Time"]):
    if (data_frame["Mean Max Temp (°C)"][index] != "M"):
        year = int(value.split("-")[0])
        month = int(value.split("-")[1])
        date = datetime.date(year, month, 1)
        x_data.append(date)
        y_data.append(data_frame["Mean Temp (°C)"][index])

        if average.get(data_frame.Year[index]) is None:
            temp = data_frame.loc[data_frame.Year == data_frame.Year[index],
                                  "Mean Temp (°C)"]
            if False not in pd.isnull(temp):
                average[data_frame.Year[index]] = temp

for val in list(average.keys()):
    average[val] = np.mean(average[val])

fig, ax = plt.subplots()

ax.plot(
    tempchange(list(average.keys()))[-15:],
    list(average.values())[-15:], "ro--")

dates = matplotlib.dates.date2num(x_data)
dates_days = np.array([(e - min(x_data)).days for e in x_data])

ax.plot(x_data[720:], y_data[720:], 'bo--')
ax.set_title("Climate in Windsor over the years")
ax.set_xlabel("Time (Years), individual points are months")
ax.set_ylabel("Average Monthly Temperature (Celsius)")
ax.grid(True, linestyle="--")
ax.legend(["Yearly Average Temperature", "Monthly Average Temperature"])

# slope, intercept, r, p_value, std_err = scipy.stats.linregress(dates_days, y_data)
# print(slope, intercept)

# ax.plot(x_data[800:], (slope*y_data+intercept)[800:], "ro--")

plt.show()
